require ["fileinto","mailbox"];

if header :contains "X-Spam-Flag" "YES" {
  fileinto :create "INBOX.Junk";
  stop;
}

if address :is "from" "paul@twisq.nl" {
  fileinto :create "INBOX.Twisq";
  stop;
}

if address :contains "from" "linkedin" {
  fileinto :create "INBOX.Amy";
  stop;
}

if address :contains "from" "quote" {
  fileinto :create "INBOX.Ads";
  stop;
}


if address :contains "from" "udemy" {
  fileinto :create "INBOX.Ads";
  stop;
}


if address :contains "from" "maartenonline" {
  fileinto :create "INBOX.Ads";
  stop;
}


if address :contains "from" "socialdeal" {
  fileinto :create "INBOX.Ads";
  stop;
}


if address :contains "from" "secretescapes" {
  fileinto :create "INBOX.Ads";
  stop;
}

if address :contains "from" "zalando" {
  fileinto :create "INBOX.Ads";
  stop;
}

