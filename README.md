# ISPmail Stretch Ansible Playbook #

This repository contains a playbook and roles that you can use to set up your own Debian-based mail server. Please follow the instructions at https://workaround.org/ispmail/stretch/ansible

# How to

The README file in the directory ansible holds instructions of how to do it.

# License #

Most of the work is done by Christoph Haas. Many thanks!

Everything in this repository can be freely used under the terms of the MIT license.
