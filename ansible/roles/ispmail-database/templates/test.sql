REPLACE INTO `mailserver`.`virtual_domains` (
      `id` ,
      `name`
)
VALUES (
      '1', '{{ mail_domain }}'
);
REPLACE INTO `mailserver`.`virtual_users` (
      `id` ,
      `domain_id` ,
      `password` ,
      `email`
)
VALUES (
      '1', '1', CONCAT('{PLAIN-MD5}', MD5( '{{ mailbox_password }}' )) , 'mail@{{ mail_domain }}'
);
REPLACE INTO `mailserver`.`virtual_aliases` (
      `id`,
      `domain_id`,
      `source`,
      `destination`
)
VALUES 
( '1', '1', 'info@{{ mail_domain }}', 'mail@{{ mail_domain }}'), 
( '2', '1', 'paul@{{ mail_domain }}', 'mail@{{ mail_domain }}');

